# -*- coding: utf-8 -*-
# ***************************************************************************
#
# ---------------------
#     begin                : 01.09.2020
#     copyright            : (C) 2019 by Ruben Müller, Büro für Angewandte Hydrologie, Berlin
#     email                : info at ruben.mueller at bah-berlin dot de
#
# ***************************************************************************
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU General Public License as published by  *
# *   the Free Software Foundation; either version 2 of the License, or     *
# *   (at your option) any later version.                                   *
# *                                                                         *
# ***************************************************************************

import datetime
import tempfile
import os
from shutil import rmtree
import webbrowser

from .owncloud import Client
from .pyqtDialogs.dialog_nietro import Ui_Dialog
from .start_figure import Figure

from qgis.core import  (Qgis, QgsProject, QgsVectorLayer, 
     QgsPalLayerSettings, QgsVectorLayerSimpleLabeling, QgsMarkerSymbol)
from qgis.utils import iface
from qgis.PyQt import QtWidgets
from qgis.PyQt.QtCore import  QDate
from qgis.PyQt.QtGui import QIcon

layer_list = ["dwd_reduced", "pegel_reduced"]
layer_id = ["DATEI", "FGWID"]
layer_id2 = [None, "DATEI"]

products_dict = {
    "Durchfluss (Szenarios mit täglicher Umhüllenden)": {"layer":layer_list[1], 
         "folder": "Q_Umhüllende_Tag", 
         "name": "quantiles_{}_{}.png",
         "legend": "legende_qe.png",
         "type": 1, "help_pdf": "Hilfe_Q_Umhüllende_Tag.pdf"},
    "Durchfluss (Szenarios monatlicher Umhüllenden)": {"layer":layer_list[1], 
          "folder": "Q_Umhüllende_Monat", 
          "name": "quantiles_{}_{}.png",
          "legend": "legende_qe.png",
          "type": 1, "help_pdf": "Hilfe_Q_Umhüllende_Monat.pdf"},
    "Durchfluss (Szenarios täglichen Zeitreihen)": {"layer":layer_list[1], 
         "folder": "Q_Zeitreihe_Tag", 
         "name": "quantiles_{}_{}.png",
         "legend": "legende_qts.png",
         "type": 1, "help_pdf": "Hilfe_Q_Zeitreihe_Tag.pdf"},
    "Durchfluss (Szenarios mit monatlichen Zeitreihen)": {"layer":layer_list[1], 
          "folder": "Q_Zeitreihe_Monat", 
          "name": "quantiles_{}_{}.png",
          "legend": "legende_qts.png",
          "type": 1, "help_pdf": "Hilfe_Q_Zeitreihe_Monat.pdf"},
    "Klimatische Wasserbilanz": {"layer":layer_list[0], 
          "folder": "KWB_rel", 
          "name": "Cwb_cum-{}.png",
          "legend": "legende_etr.png",
          "type": 2, "help_pdf": "Hilfe_Evapotranspiration.pdf"},
    "Niederschlag (längjährige Zeitreihe)": {"layer":layer_list[0], 
           "folder": "Niederschlag_langjährig", 
           "name": "PI_cum-{}.png",
           "legend": "legende_p.png",
           "type": 3, "help_pdf": "Hilfe_Niederschlag_Zeitreihe.pdf"},
    "Niederschlag (Jahresgang)": {"layer":layer_list[0], 
           "folder": "Niederschlag_saisonal", 
           "name": "PI_cum-{}.png",
           "legend": "legende_pm.png",
           "type": 3, "help_pdf": "Hilfe_Niederschlag_Saisonal.pdf"},
    "Dürreindikator Bodenfeuchte": {"layer": None, 
         "folder": "Dürre_Bodenfeuchte", 
         "name": "bf_T-1.png", 
         "legend": "legende_ufz_duerre.png",
         "type": 0, "help_pdf": "Hilfe_Bodenfeuchte.pdf"},
    "Speicherinhalt des Bodens": {"layer": None, 
         "folder": "Speicherinhalt_Boden", 
         "name": "quantiles_A.png", 
         "legend": "legende_spi.png",
         "type": 0, "help_pdf": "Hilfe_SPI.pdf"},
    "Speicherinhalt der Oberfläche": {"layer": None, 
         "folder": "Speicherinhalt_GEW", 
         "name": "quantiles_Q.png", 
         "legend": "legende_spi.png",
         "type": 0, "help_pdf": "Hilfe_SPI.pdf"},
    "Speicherinhalt des Grundwassers": {"layer": None, 
         "folder": "Speicherinhalt_GW",
         "name": "quantiles_G.png", 
         "legend": "legende_spi.png",
         "type": 0, "help_pdf": "Hilfe_SPI.pdf"},
    }


def check_layer_loaded(layer_name):
    """Check if layer with layer_name is loaded.

    Parameters
    ----------
    layer_name : str
        the name of the layer

    Returns
    -------
    tmp : bool
        True if layer is registered
    """
    boolean = False
    layers = QgsProject.instance().mapLayers()
    for name, layer in layers.items():
        if layer.name() == (layer_name):
            boolean = True
            break
    return boolean


def load_layer(layer_name):
    """Load an shape layer_name.

    Parameters
    ----------
    layer_name : str
        the name of the layer

    Returns
    -------
    layer : layer object
        the loaded layer
    """
    pluginPath = os.path.dirname(__file__)
    
    try:
        if not check_layer_loaded(layer_name):
            layer = QgsVectorLayer(os.path.join(pluginPath, "maps", layer_name + ".shp"), layer_name, "ogr")
            QgsProject.instance().addMapLayer(layer)
        else:
            layer = QgsProject.instance().mapLayersByName(layer_name)[0]
        return layer
    except:
        iface.messageBar().pushMessage('Konnte Layer {} nicht laden.'.format(layer_name),
                                        level=Qgis.Warning, duration=7)
  



class Nietro(QtWidgets.QDialog, Ui_Dialog):
    '''Class to show the NieTro plugin'''
    
    
    def __init__(self, parent=None):
        super().__init__(parent.mainWindow())
        self.setupUi(self)
        self.calendarWidget.setGridVisible(True)
        self.max_date()
        self.pushButton.clicked.connect(self.show_figure)
        self.pluginPath = os.path.dirname(__file__)
        self.active_layer = "_-_"*8
        self.figure = Figure(self)
        self.save_folder = os.path.join(tempfile.gettempdir(), '.{}'.format(hash(os.times())))
        self.a_tmpfile = True
        self.file_tmp_handle = None
        self.pushButton_folder.clicked.connect(self.select_save_folder)
        self.pushButton_2.clicked.connect(self.load_maps)
        self.pushButton_help.clicked.connect(self.select_help)
        self.pushButton_help.setIcon(QIcon(os.path.join(self.pluginPath, 'icons/help.png')))
        iface.currentLayerChanged.connect(self.change_ddm)
        self.change_ddm()
        self.oc = Client.from_public_link("https://cloud.kraut.solutions/index.php/s/beAqjijiFB7grSy", 
            "NieTro??")
        
        
    def load_maps(self):
        
        layer = ("pegel_reduced", "dwd_reduced")
        fields = ("PEGELNAME", "STATIONSNA")
        color = ("red", "green")
        
        for i, cln in enumerate(layer):
            cl = load_layer(cln)
            settings = QgsPalLayerSettings()
            settings.fieldName = fields[i]
            labeling = QgsVectorLayerSimpleLabeling(settings)
            cl.setLabelsEnabled(True)
            cl.setLabeling(labeling) 
            cl.triggerRepaint()
            symbol = QgsMarkerSymbol.createSimple({'color': color[i]})
            cl.renderer().setSymbol(symbol)
        
    def select_save_folder(self):
        """Show dialog for folder selection."""
        foldername = QtWidgets.QFileDialog.getExistingDirectory(None, 'Ausgabeverzeichnis')
        if foldername:
            self.save_folder = foldername
            self.a_tmpfile = False


    def max_date(self):
        """Check time. 
        Data is available approx. 14:30h UTC each day, allow data acquisition for the current day only after this daytime."""
        self.now = self.now = datetime.datetime.utcnow()
        self.now_m1 = datetime.datetime.utcnow() - datetime.timedelta(days=1)
        mnow = self.now if self.now.hour >= 16 and self.now.minute >= 30 else self.now_m1
        print(mnow)
        self.calendarWidget.setMaximumDate(QDate(mnow.year, mnow.month, mnow.day))
        
        
    def change_ddm(self):
        """change the drop-down menue"""
        if self.isVisible():
            self.ddm()


    def ddm(self):
        self.max_date()
        
        if iface.activeLayer() is None:
            return
        if iface.activeLayer().name() in layer_list: 
            self.active_layer = iface.activeLayer().name()
            self.comboBox.clear()
            [self.comboBox.addItem(x[0]) for x in products_dict.items() \
                if x[1]["layer"] is None or x[1]["layer"] == self.active_layer]
  
                
    def show_figure(self):
        """download the data and show the figure"""
        product = self.comboBox.currentText()
        if iface.activeLayer() is None:
            return
        sfeatures = iface.activeLayer().selectedFeatures()
        
        skip_check = product in ("Dürreindikator Bodenfeuchte",
            "Speicherinhalt des Bodens", "Speicherinhalt der Oberfläche", 
            "Speicherinhalt des Grundwassers")
        
        date_folder = self.calendarWidget.selectedDate().toString("yyyyMMdd")
        ptype = products_dict[product]["type"]
        
        if not skip_check:
            if len(sfeatures) > 2:
                iface.messageBar().pushMessage(u"{} ausgewählt! Nur eine Selektion zulässig".format(len(sfeatures)),
                      level=Qgis.Info, duration=3)
                return
    
            if not len(sfeatures):
                iface.messageBar().pushMessage(u"Keine Selektion vorgenommen.",
                      level=Qgis.Info, duration=1)
                return
        
            ln = iface.activeLayer().name()

            if iface.activeLayer().name() in layer_list:
                
                feat = sfeatures[0]
                
                idx_layer = layer_list.index(ln)
                idx_id = iface.activeLayer().fields().lookupField(layer_id[idx_layer])
                feat_id =  feat[idx_id]
                    
                if ptype == 1:
                    if not ln == layer_list[1]:
                        iface.messageBar().pushMessage(u"Produkt und Layer passen nicht",
                              level=Qgis.Info, duration=3)
                        return
                    idx_id2 = iface.activeLayer().fields().lookupField(layer_id2[idx_layer])
                    feat_id2 =  feat[idx_id2]
                elif ptype in (2, 3):
                    if not ln == layer_list[0]:
                        iface.messageBar().pushMessage(u"Produkt und Layer passen nicht",
                                  level=Qgis.Info, duration=3)
                        return

        if ptype == 0: ### fixed names for all None at layer_id2[0]
            print(0)
            name = products_dict[product]["name"]
        elif ptype == 1: ### flows feature gauge name and fgwid
            print(1)
            name = products_dict[product]["name"].format(feat_id2, feat_id)
        elif ptype == 2: ### meteo product **without** leading zeros
            print(2)
            name = products_dict[product]["name"].format(feat_id.lstrip("0"))
        else: ### meteo product **with** leading zeros
            print(3)
            name = products_dict[product]["name"].format(feat_id)

        url_part2 = '/{}/{}/{}'.format(products_dict[product]["folder"],
            date_folder, name)

        if self.a_tmpfile:
            if not os.path.isdir(self.save_folder):
                os.makedirs(self.save_folder)
            
        file_tmp = os.path.join(self.save_folder, url_part2.replace("/", "_"))

        try:
            if not os.path.isfile(file_tmp):
                self.oc.get_file(url_part2, local_file=file_tmp)
        except Exception as e:
            if e == "HTTP error: 404":
                iface.messageBar().pushMessage(u"Fehler beim Download: Datei nicht gefunden",
                    level=Qgis.Warning, duration=5)
            else:
                iface.messageBar().pushMessage(u"Fehler beim Download: %s" % e,
                    level=Qgis.Warning, duration=5)
            return
            
        self.figure.prep_figure([file_tmp, 
            os.path.join(self.pluginPath, "help", products_dict[product]["legend"])])
        self.figure.show()
        
        
    def select_help(self):
        product = self.comboBox.currentText()
        pdf_file = products_dict[product]["help_pdf"]
        webbrowser.open(os.path.join(self.pluginPath, "help", pdf_file))
        

    def closeEvent(self, event):
        try:
            if self.a_tmpfile:
                rmtree(self.save_folder, ignore_errors=True)
        except Exception as e:
            print(e)
        

    def showEvent(self, event):
        self.ddm()
                   
            
            
            

        
        
        
        
        
 
        
