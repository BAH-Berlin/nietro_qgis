# -*- coding: utf-8 -*-
# ***************************************************************************
#
# ---------------------
#     begin                : 01.09.2020
#     copyright            : (C) 2019 by Ruben Müller, Büro für Angewandte Hydrologie, Berlin
#     email                : info at ruben.mueller at bah-berlin dot de
#
# ***************************************************************************
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU General Public License as published by  *
# *   the Free Software Foundation; either version 2 of the License, or     *
# *   (at your option) any later version.                                   *
# *                                                                         *
# ***************************************************************************
	

from PyQt5.QtWidgets import QDialog, QVBoxLayout, QLabel, QWidget, QPushButton
from .start_figure_legend import Figure_legend
from PyQt5.QtGui import QPixmap, QPainter
from PyQt5.QtCore import Qt

class Label(QWidget):
    """Class to draw a png."""
    def __init__(self, parent=None):
        QWidget.__init__(self,)
        self.p = QPixmap()

    def setPixmap(self, p):
        self.p = p
        self.update()

    def paintEvent(self, event):
        if not self.p.isNull():
            painter = QPainter(self)
            painter.setRenderHint(QPainter.SmoothPixmapTransform)
            painter.drawPixmap(self.rect(), self.p)


class Figure(QWidget):
    """Class to show the figure window."""
    def __init__(self, parent=None):
        QWidget.__init__(self)
        self.parent = parent
        self.lay = QVBoxLayout(self)
        self.resize(800, 600);
        self.lb = Label(self)
        self.fl = Figure_legend(self)
        
        self.pushButton_legend = QPushButton()
        self.pushButton_legend.setObjectName("pushButton_legend")
        self.pushButton_legend.clicked.connect(self.show_legend)
        self.pushButton_legend.setText("Legende")
        self.setWindowModality(Qt.ApplicationModal) 

    def prep_figure(self, figure):
        """Prepare to draw the png."""
        self.lb.setPixmap(QPixmap(figure[0]))
        self.fl.prep_figure(figure[1])
        self.lay.addWidget(self.lb)
        self.lay.addWidget(self.pushButton_legend)
        
    def show_legend(self):
        self.fl.show()
        
