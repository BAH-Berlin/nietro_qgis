# -*- coding: utf-8 -*-
# ***************************************************************************
#
# ---------------------
#     begin                : 01.09.2020
#     copyright            : (C) 2019 by Ruben Müller, Büro für Angewandte Hydrologie, Berlin
#     email                : info at ruben.mueller at bah-berlin dot de
#
# ***************************************************************************
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU General Public License as published by  *
# *   the Free Software Foundation; either version 2 of the License, or     *
# *   (at your option) any later version.                                   *
# *                                                                         *
# ***************************************************************************
	

from PyQt5.QtWidgets import QDialog, QVBoxLayout, QLabel, QWidget
from PyQt5.QtGui import QPixmap, QPainter
from PyQt5.QtCore import Qt

class Label(QWidget):
    """Class to draw a png."""
    def __init__(self, parent=None):
        QWidget.__init__(self,)
        self.p = QPixmap()

    def setPixmap(self, p):
        self.p = p
        self.update()

    def paintEvent(self, event):
        if not self.p.isNull():
            painter = QPainter(self)
            painter.setRenderHint(QPainter.SmoothPixmapTransform)
            painter.drawPixmap(self.rect(), self.p)


class Figure_legend(QWidget):
    """Class to show the figure window."""
    def __init__(self, parent=None):
        QWidget.__init__(self)
        self.parent = parent
        self.lay = QVBoxLayout(self)
        self.resize(560, 360)
        self.lb = Label(self)
        self.setWindowModality(Qt.ApplicationModal) 
 
    def prep_figure(self, figure):
        """Prepare to draw the png."""
        print(figure)
        self.lb.setPixmap(QPixmap(figure))
        self.lay.addWidget(self.lb)

