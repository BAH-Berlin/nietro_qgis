# NieTro Plugin for Qgis 

![plugin_dialog](_static/nietro-viewer.png)


## HowTo
This is the experimental QGIS3-plugin of the NieTro project.
  
   1. Load the maps
   2. Select the map for gauges or meteorological stations 
   3. Select a meteorological station or gauge
   4. Specify a date. 
   5. Optional: Select a download folder, otherwise pictures will be saved only temporary.
   6. Show

The plugin will download and show the figure from the Kraut cloud (sponsored by Kraut.Hosting GmbH, https://kraut.hosting/).


## Author
Ruben Müller  
Büro für Angewandte Hydrologie, Berlin  
software@bah-berlin.de
[NieTro](www.bah-berlin.de)  
[BAH-Berlin](www.bah-berlin.de)  


![BAH_logo](_static/bah-logo.png)
