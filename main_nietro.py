# -*- coding: utf-8 -*-
# ***************************************************************************
#
# ---------------------
#     begin                : 01.09.2020
#     copyright            : (C) 2019 by Ruben Müller, Büro für Angewandte Hydrologie, Berlin
#     email                : info at ruben.mueller at bah-berlin dot de
#
# ***************************************************************************
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU General Public License as published by  *
# *   the Free Software Foundation; either version 2 of the License, or     *
# *   (at your option) any later version.                                   *
# *                                                                         *
# ***************************************************************************


import os
from qgis.core import Qgis
from qgis.PyQt.QtWidgets import QAction
from qgis.PyQt.QtGui import QIcon

pluginPath = os.path.dirname(__file__)

from .start_nietro import Nietro

class Main:
    '''class for the plugin'''
    def __init__(self, iface):
        super(Main, self).__init__()
        ### attributes
        self.iface = iface
        self.nietro = Nietro(iface)
        self.iface.messageBar().pushMessage('Plugin geladen',
                                            level=Qgis.Info, duration=3)
    
    def initGui(self):
        self.action = QAction(QIcon(os.path.join(pluginPath, 'icons', 'icon.png')), "Nietro", self.iface.mainWindow())
        self.action.setWhatsThis(u"Nietro")
        self.action.setStatusTip(u"Nietro")
        self.action.triggered.connect(self.nietro.show)
        self.iface.addToolBarIcon(self.action)
        self.iface.addPluginToMenu("&BAH", self.action)


    def unload(self):
        self.iface.removePluginMenu("&BAH", self.action)
        self.iface.removeToolBarIcon(self.action)
    

