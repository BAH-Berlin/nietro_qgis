# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'figure_legend.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog_figure(object):
    def setupUi(self, Dialog_figure):
        Dialog_figure.setObjectName("Dialog_figure")
        Dialog_figure.resize(322, 295)
        self.gridLayout = QtWidgets.QGridLayout(Dialog_figure)
        self.gridLayout.setObjectName("gridLayout")
        self.graphicsView = QtWidgets.QGraphicsView(Dialog_figure)
        self.graphicsView.setObjectName("graphicsView")
        self.gridLayout.addWidget(self.graphicsView, 0, 0, 1, 1)

        self.retranslateUi(Dialog_figure)
        QtCore.QMetaObject.connectSlotsByName(Dialog_figure)

    def retranslateUi(self, Dialog_figure):
        _translate = QtCore.QCoreApplication.translate
        Dialog_figure.setWindowTitle(_translate("Dialog_figure", "Dialog"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Dialog_figure = QtWidgets.QDialog()
    ui = Ui_Dialog_figure()
    ui.setupUi(Dialog_figure)
    Dialog_figure.show()
    sys.exit(app.exec_())

